% -*- coding: utf-8 -*-
\documentclass[runningheads]{llncs}
\usepackage[
    pdfpagescrop={92 52 523 730},
    colorlinks=true,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black,
    breaklinks=true,
    ]{hyperref}

\newcommand{\speaker}[1]{\textbf{#1:} }

\title{From Secure Messaging to Secure Collaboration\\(Transcript of Discussion)}
\author{Martin Kleppmann, Stephan A.\ Kollmann,\\Diana A.\ Vasile, and Alastair R.\ Beresford}
\authorrunning{M.\ Kleppmann, S.\,A.\ Kollmann, D.\,A.\ Vasile, A.\,R.\ Beresford}
\institute{Department of Computer Science and Technology, University of Cambridge, UK}
\date{}

\begin{document}
\maketitle

\speaker{Frank Stajano}
Among the people who come up with these conflict resolution algorithms, is there agreement about what to do in a situation where the edits look fundamentally incompatible to a normal person?
For example, while we are offline, I delete a paragraph, whereas you add a word in the middle of that paragraph.

\speaker{Reply}
That's a good question. There are a few different consistency models that people use, but on the whole, they are fairly primitive in terms of what conflict resolution they do. In your example, where one user deletes an entire paragraph, another user changes something in the middle of that paragraph --- I actually tried this with Google Docs. The merged result is a document in which that paragraph is missing, but that one changed word from the middle of the paragraph is still there. So you've essentially got a `stranded' word in the middle of the document.

\speaker{Frank Stajano}
Are real human beings happy with this way of resolving?

\speaker{Reply}
Well, millions of people seem to be using Google Docs successfully, so I'm just going to assert that it seems to be good enough in practice. It would be nice to have a user interface that warns you, saying: ``Hey, there were some edits made here to the same place in the document. You might want to check this is a valid, merged result.'' All the algorithms do is to ensure that everyone ends up in the same state, and that state doesn't lose any data, if possible. 

\speaker{Ilia Shumailov}
I think the general flow of action is, because they keep the history of changes, you can always just go back to another change and re-merge the text yourself. At least that's what I do every time I have a conflict.

\speaker{Reply}
Yes indeed. They keep the editing history, and you can look backward in the editing history as well. However, from the point of view of our discussion today, take conflict resolution as a solved problem. I'm just going to assert that these algorithms work, under the assumption that the same set of messages are delivered to everybody in the group.

We have proved that for several of these protocols, if several nodes have seen the same set of operations, but maybe in a different order, then they converge towards the same state. What we need to ensure now in the protocol is that all of the participants see the same set of operations, and that's where security protocols come in. 

\emph{(Presentation continues, describing the threat model and security objectives from Section 2 of the paper.)}

\speaker{Ian Goldberg}
When you define confidentiality rigorously, it's not enough to say that non-collaborators cannot access the data, because you could have a non-collaborator colluding with a collaborator. So you have to rule out colluding collaborators, and then it gets really complicated really fast. It's not only a closed group, you have to deal with what happens when people leave the group, or what happens if you have two colluding people in the group and one of them leaves the group. How do you stop that person?

\speaker{Reply}
If there's existing literature on defining these things, I'd love to hear more about that. 

\speaker{Ian Goldberg}
It's very complicated. And just in the context of group messaging, this is active research right now and my group works on that exact thing. And you have problems involving adding and removing group members.

In the secure messaging realm, what you're talking about is sometimes called \emph{transcript consistency}, and then there are a couple of different kinds of transcript consistency. There is \emph{global transcript consistency}, which is a guarantee that everyone in the group sees exactly the same messages in the same order. That's very hard to do, and the UI for that is ridiculous because you may actually receive messages out of order, but then some BFT-like protocol runs and it says, ``oh, that message really should have gone there''. And so what is the UI going to do? You see messages and then this one jumps up four messages? It's kind of crazy.

\speaker{Reply}
Yeah, what we want here is a weaker kind of transcript consistency, which doesn't enforce ordering, just enforces that the set of messages is the same.

\speaker{Ian Goldberg}
Right. You probably do still want \emph{causal transcript consistency}, which means a reply to any message will necessarily appear after that message.

\speaker{Reply}
Yes, but that's much cheaper to achieve, I think. 

\speaker{Ian Goldberg}
Exactly, it's much easier.

\speaker{Frank Stajano}
I have somewhat similar comments with respect to integrity, where the fact that non-collaborators cannot modify the content is necessary but insufficient. For example, if David and I have a shared document containing the expense reports for our company, and both of us are collaborators and authorised, but neither of us should be able to go back and change some expenses that we've already approved. There may be many other integrity properties of the document besides the fact that people who are not collaborators should not be able to modify. 

\speaker{Reply}
Yes, absolutely. Underneath the shared document is a message log containing all of the changes that ever happened. And you can have stronger integrity properties on that log, so you would not be able to tamper with the message log, for example. 

\emph{(Presentation continues with Sections 3 and 4 of the paper, and posing the problem of checking the integrity of snapshots.)}

\speaker{Daniel Weitzner}
It's an interesting problem. I wonder if you have a bit of a concurrency-versus-privacy problem? If the first two users are still actively editing and at some point~-- as I think Ilia suggested~-- they want to go back in history, then what about the user who was invited later? Does the user who joined later get to see the history after whatever snapshot you did? Do you end up with two different versions of the document based on when you joined? If you've flattened out all the state then you've lost a lot of information for the first two users.

\speaker{Reply}
Yes, potentially. In particular, if there's some editing happening concurrently to the state snapshot being taken---

\speaker{Daniel Weitzner}
Or just afterwards. It doesn't even need to---

\speaker{Reply}
Yeah, ``concurrently'' in the distributed sense, that means, they're not aware of each other. Then yes, it could absolutely happen that this other edit can't be applied to that state, because it's assuming some kind of metadata that has been flattened out. That's absolutely a potential problem, yeah. 

\speaker{Frank Stajano}
You say it's a problem that two thirds of the participants have to be honest, and online. I'm not sure about online, but is it not going to be the case that, in order to accept the newly invited guy, the previous guys have to agree to let him in? And therefore, at the time they say ``okay, I think the new guy is alright'', they could also say ``this is the snapshot'' (or hash of snapshot or something like that) that could contribute to the new guy having a view that they concur on.

\speaker{Reply}
I guess it depends what kind of permission you want to require for new people to be added. At least with Google Docs, and I think with most of the group messaging protocols, any one of the existing participants can just unilaterally invite a new person. So they don't require any kind of agreement from the group---

\speaker{Frank Stajano}
If this is the protocol, how can you even start worrying about privacy if any guy around can invite any of their pals? Then of course privacy goes out the window, right?

\speaker{Reply}
Well no, it's already the case that any one user, if they want to, can just send a copy of the document to their mates, completely outside of the protocol. There's no way of constraining what these individual people can do with their decrypted copy of the data. 

\speaker{Frank Stajano}
Then why are you worried that they might see a past edit, if any of these guys can send it anyway, by your own assertion?

\speaker{Reply}
Well, the intention is that if the existing users don't want to share the past state, they have a way of sharing only the current state. Of course, if they wanted to leak the past state they could do that, but we are trying to avoid inadvertently leaking that past state when inviting a new user.

\speaker{Ilia Shumailov}
I think the goal is to save yourself from the server, not from the collaborators. 

\speaker{Frank Stajano}
He's eliminated the server, hasn't he?

\speaker{Ilia Shumailov}
Yeah, but how do you keep it consistent? 

\speaker{Frank Stajano}
Right, yes.

\speaker{Reply}
Keep what consistent? 

\speaker{Ilia Shumailov}
The version of the document, all operations.

\speaker{Reply}
These algorithms will work perfectly fine without a server because they're tolerant of messages arriving out of order. You can run this on top of peer-to-peer protocol without any problems really. 

\speaker{Ilia Shumailov}
Yeah this is just an optimisation not to share all of the operations for all of the documents with every new peer. Another interesting question would then be: what happens when the person who was offline joins in with a completely separate state of operations? Does that imply that, in order to impose new rules as a malicious user, you start DoSing all of the legitimate users? And then all of the non-legitimate users can form a current consensus with the online users, and distribute their own version of the document. 

\speaker{Reply}
I guess that could happen, yes. I hadn't really considered users DoSing each other, but it's conceivable. 

\speaker{Alexander Hicks}
This question is related to what you were discussing with Frank a few minutes ago. If you only want to share the latest state, I'm going to take the example of Google Docs here --- it's a bit contrived, but can't you just create a new document, copy and paste what you want, and you've shared the latest state successfully? And you can even have access to your past edits by going back to your other doc if you want, without any risks. Obviously, it's maybe not practical to always open a new document, but it seems like it's doable at least.

\speaker{Reply}
Yes, the only problem is that there is no way for the new participant to tell whether the new copy of the document, that copy-and-pasted document, is actually consistent with what happened previously. So the person who does the copying and pasting of the document may well manipulate the document at the same time, and there is nothing stopping them.

\speaker{Alexander Hicks}
True, but then I guess from there on they would be satisfied they have the latest copy. Unless they're assuming that you're also editing the other copy, but then you can't really avoid that anyway.

\speaker{Reply}
But if you want the existing collaborators to still be able to continue editing, all three have now become equal collaborators, so you want to ensure that all three of them are in the same state. If you copy and paste the entire document into a new one, that means that all of the other participants also need to switch over to the new one, and presumably the participants would, in the process, do some comparison and checking whether the document still agrees with what they thought it was before the copy-and-paste.

\speaker{Alexander Hicks}
Sure, but you could probably generate some proof of that without necessarily revealing the past changes. 

\speaker{Reply}
Well yes, that's exactly what we're trying to work out. Are there cryptographic techniques that we can use to prove the integrity?

\speaker{Alexander Hicks}
Something like a light client, I guess.

\speaker{Reply}
Like what?

\speaker{Alexander Hicks}
I guess maybe Paddy [Patrick McCorry] can say\dots\ for Bitcoin you have light clients, which only verify up to the past few blocks --- is that correct? Something like that could probably work. 

\speaker{Reply}
Yes, sounds conceivable. I don't know very much about Bitcoin. 

\speaker{Ian Goldberg}
At the end of the day, you can always just run a SNARK, right? So you have a circuit that applies a change to a state, yielding another state, and the SNARK just checks that that was done correctly. It will not be cheap to generate, but it will be cheap to check. And then you can just, along with the latest state, carry a proof that this latest state was generated correctly, without revealing any of the inputs.

\speaker{Reply}
Yes, that would be interesting to try.

\speaker{Patrick McCorry}
This is sort of similar to state channels, where everyone agrees on the new state, so it's $n$ out of $n$ -- everyone has to sign it. And when the new person joins, everyone has to agree to that as well. Is it the same process here when someone joins the collaboration? Do you require everyone's authorisation?  

\speaker{Reply}
No. At the moment, the way we're thinking about this is that any one member of the group can add new members without having to coordinate with the others. That works nicely if these are mobile devices that are offline most of the time, where we really don't want to have to wait for someone else to come online. Even more so if, for example, one of the devices has been dropped in the toilet by somebody, and so it is never going to come back online again. In that case, we can at most wait for some kind of quorum, but we wouldn't want to have to wait for everybody. 

\speaker{Ilia Shumailov}
Well, then that definitely completely destroys your technique here. You said that you wanted two out of three legitimate users. Then you just add a lot of users who have copied state from you, and just say ``okay, this is the new state''. Right? 

\speaker{Reply}
Yes certainly, if you allow arbitrary Sybil identities to be created, then any sort of majority voting seems a bit meaningless. Though the nice thing with these redactable signatures is that they don't depend on any majorities: we can verify the signatures with respect to all of the previous users without waiting for any communication with them. 

\speaker{Ilia Shumailov}
Oh, so that implies that with each one of them, you actually make a confirmation that this is the document.

\speaker{Reply}
Yes. 

\speaker{Ilia Shumailov}
Well, does that not imply that if I add a bunch of copies of myself and claim that this is the new document, it's still going to work? What happens in the case of conflict?

\speaker{Reply}
Editing conflicts within the document are on a separate layer above this. Here, at this layer, we just need to ensure that everyone sees the same set of messages. 

\speaker{Ilia Shumailov}
So, if I clone myself a number of times, and then I say, ``okay, this is the new document'', what happens with the guy who comes in?

\speaker{Reply}
In this snapshot, part of the information are the user IDs of who wrote what. Those user IDs might be a hash of their public key, for example. So if you don't have the private key for the other participants, you wouldn't be able to impersonate edits from the other people. You could still make a brand new document, in which only you have made the edits, and there have been no edits from others. For that document you would still need some kind of checking with the other participants, and they would at that point say: ``hey no, this isn't the document that we were working on -- this is something completely different''.

\end{document}

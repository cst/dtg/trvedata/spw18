\documentclass[runningheads]{llncs}
\usepackage[utf8]{inputenc}
\usepackage[
    pdfpagescrop={92 52 523 730},
    colorlinks=true,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=blue,
    breaklinks=true,
    ]{hyperref}

% Need to include this, according to instructions in the LLNCS sample paper
\renewcommand\UrlFont{\color{blue}\rmfamily}

\usepackage{tikz}
\usetikzlibrary{arrows.meta}
\hyphenation{Whats-App}

\begin{document}
\title{From Secure Messaging to Secure Collaboration}
\author{Martin Kleppmann \and Stephan A.\ Kollmann \and\\ Diana A.\ Vasile \and Alastair R.\ Beresford}
\authorrunning{M.\ Kleppmann, S.\,A.\ Kollmann, D.\,A.\ Vasile, A.\,R.\ Beresford}
\institute{Department of Computer Science and Technology, University of Cambridge, UK
\email{\{mk428,sak70,dac53,arb33\}@cl.cam.ac.uk}}
\maketitle

\begin{abstract}
We examine the security of collaboration systems, where several users access and contribute to some shared resource, document, or database.
To protect such systems against malicious servers, we can build upon existing secure messaging protocols that provide end-to-end security.
However, if we want to ensure the consistency of the shared data in the presence of malicious users, we require features that are not available in existing messaging protocols.
We investigate the protocol failures that may arise when a new collaborator is added to a group, and discuss approaches for enforcing the integrity of the shared data.
\end{abstract}

\keywords{group communication \and collaborative editing \and secure messaging}

\section{Introduction}

Secure messaging apps with end-to-end encryption, such as Signal, WhatsApp, iMessage and Telegram, have broken into the mainstream: for example, WhatsApp alone has 1.3 billion monthly active users~\cite{WhatsApp:2017}.
The success of these apps demonstrates that it is feasible for protocols with strong security properties to be deployed at internet-wide scale, and that their benefits can be enjoyed by users who are not technical experts.

However, secure messaging alone is not sufficient for protecting all forms of sensitive data exchange.
Some communication takes the form of collaborating on some shared resource, such as a document, or database. For example, journalists collaborate on sensitive investigations by interviewing sources, analysing documents, and sharing their notes and drafts with colleagues~\cite{McGregor:2015vr,McGregor:2017vu}; lawyers collaborate on contracts and other sensitive documents while communicating with their clients under legal privilege~\cite{Lerner:2017tl}; and medical records are maintained by several specialists involved in treating a patient.
Most currently deployed systems for these forms of collaboration rely on a server that is trusted to maintain the confidentiality and integrity of the data.

In this paper we discuss how existing protocols for secure messaging can be leveraged to bring end-to-end security to scenarios in which several users collaborate on a database or a set of shared documents.
We give a brief overview of existing algorithms and technologies, report on lessons learnt from our initial implementation of secure collaboration software, and highlight some open problems that we hope will stimulate further work in the information security community.

\section{Threat Model and Security Objectives}\label{sec:threatmodel}

We assume that the collaboration software has any number of \emph{users}, each of whom may have one or more \emph{devices} (which may be desktop or laptop computers, or mobile devices such as tablets and smartphones).
Users and their devices may form groups of \emph{collaborators}, and the collaborators in each group have shared access to a particular document or dataset.
Each collaborating device maintains a copy (replica) of the shared data on its local storage.

Devices may frequently be \emph{offline} and unable to communicate, for example because they might be mobile devices with poor cellular data coverage.
We require that devices should be able to modify their local copy of the data even while offline, and send their changes to other devices when they are next online.

The system may also include some number of \emph{servers}, which store messages for any recipient devices that are offline, and forward them to those devices when they are next online.
Devices may communicate with each other directly (e.g.\ via a LAN, Bluetooth, or peer-to-peer over the Internet), or indirectly via servers.
Furthermore we assume the existence of a public key infrastructure (PKI) through which users and devices can authenticate each other.

We consider the following types of adversary:
\begin{description}
\item[Network Attacker.] This adversary has full control over any network via which devices communicate, including the ability to observe and modify all traffic.
\item[Malicious Server.] This adversary controls any messages sent via or stored on a server, including the ability to observe and modify any messages.
\item[Malicious User.] This adversary is able to create any number of devices that may participate in group collaboration, and which may deviate arbitrarily from the protocol specification.
\end{description}
In the face of these adversaries we seek the following security properties:
\begin{description}
\item[Confidentiality.] The data shared between a group of collaborators cannot be obtained by an adversary who is not a member of that group.
\item[Integrity.] The data shared between a group of collaborators cannot be modified by an adversary who is not a member of that group.
\item[Closeness.] A user or device can become a group member only by explicitly being added by a group administrator.
\item[Convergence.] When any honest group members communicate, their local copies of the shared data converge towards a consistent state (even if some other group members are malicious).
\end{description}

We propose encoding the shared data and any modifications as messages, and using a secure group messaging protocol to exchange them among collaborators.
Existing secure group messaging protocols maintain the confidentiality and integrity properties in the presence of all types of adversary~\cite{CohnGordon:2017ue,Unger:2015kg}.
Closeness is sometimes weaker in existing protocols: for example, WhatsApp does not guarantee closeness in the presence of a malicious server~\cite{Rosler:2018bq}.
However, group key agreement protocols that ensure closeness have been studied previously~\cite{Kim:2001eh}, so we do not consider this property further in this paper.

Thus, when building a secure collaboration protocol on top of a secure messaging protocol, the primary security goal is to ensure \emph{convergence} in the presence of the aforementioned adversaries.

\section{Convergence of Shared State}\label{sec:convergence}

Since we allow the data on a device's local storage to be modified while the device is offline, independent modifications on different devices can cause their copies of the shared data to become inconsistent with each other.
Fortunately, this problem has been studied extensively in the distributed systems literature.
We propose using \emph{Conflict-free Replicated Data Types} or \emph{CRDTs}~\cite{Kleppmann:2016ve,Shapiro:2011un}, a family of algorithms that provide abstractions and protocols for automatically resolving conflicts due to concurrent modifications.

CRDTs provide a consistency property called \emph{strong eventual consistency}, which guarantees that whenever any two devices have seen the same set of updates (even if the updates were delivered in a different order), the data on those devices is in the same state~\cite{Gomes:2017gy,Shapiro:2011un}.
This property implies that the state of a device is determined entirely by the set of updates it has seen.

Thus, we can achieve the convergence property for collaborative data by encoding every update as a message and sending it to other devices via a secure group messaging protocol.
On each device, we use a CRDT to interpret the set of messages delivered to that device, and derive its local copy of the shared data from those messages.
Now, the problem of achieving convergence is reduced to ensuring that all honest group members receive the same set of messages.

In the context of secure messaging protocols, ensuring that group members receive the same set of messages is known as \emph{transcript consistency}~\cite{CohnGordon:2017ue,Unger:2015kg}.
(Sometimes transcript consistency is taken to mean that all group members must receive the same sequence of messages in the same order; for our purposes, it is sufficient to require the weaker property that collaborators must receive the same set of messages, regardless of order.)
Not all messaging protocols provide this property; for example, Signal does not ensure transcript consistency in the presence of a malicious user~\cite{Rosler:2018bq}.
However, the property can be implemented as a separate layer on top of an existing messaging protocol.

A simple approach based on a hash function is illustrated in Figure~\ref{fig:hash-chain}: whenever a device sends a message to the group (e.g.\ message $m_4$), it includes a hash of the last message it sent ($m_2$), and the hashes of any other messages it received in the intervening period ($m_3$).
A recipient accepts an incoming message only after it has received all prior messages that it depends on, which are referenced by their hashes.
Assuming preimage resistance of the hash function, whenever two devices observe the same message, then they must have also received the same set of prior messages (namely those that are transitively reachable through their hash references).

\begin{figure}
\centering
\begin{tikzpicture}
  \tikzstyle{box}=[draw,anchor=base,minimum width=12mm,text height=6pt,text depth=2pt,font=\scriptsize]
  \matrix [column sep={12mm,between origins},matrix anchor=west] at (0,2) {
    \node [left,font=\footnotesize] {$m_1$}; &&
      \node [left,font=\footnotesize] {$m_2$}; &&&
      \node [left,font=\footnotesize] {$m_4$}; \\
    \node [box] (m1) {$\mathit{update}_1$}; &&
    \node [box] (m2a) {$H(m_1)$}; & \node [box] (m2b) {$\mathit{update}_2$}; &&
    \node [box] (m4a) {$H(m_2)$}; & \node [box] (m4b) {$H(m_3)$}; &
      \node [box] (m4c) {$\mathit{update}_4$}; \\
  };
  \matrix [column sep={12mm,between origins},matrix anchor=west] at (2.2,0) {
    \node [box] (m3a) {$H(m_1)$}; & \node [box] (m3b) {$\mathit{update}_3$}; &&
    \node [box] (m5a) {$H(m_3)$}; & \node [box] (m5b) {$H(m_2)$}; &
      \node [box] (m5c) {$\mathit{update}_5$}; &&
      \node [box] (m6) {\dots}; \\
    \node [left,font=\footnotesize] {$m_3$}; &&&
      \node [left,font=\footnotesize] {$m_5$}; \\
  };
  \draw [->] (m2a) -- (m1);
  \draw [->] (m3a.west) -- (m1.south east);
  \draw [->] (m4a) -- (m2b);
  \draw [->] (m4b.south) -- (m3b.north east);
  \draw [->] (m5a) -- (m3b);
  \draw [->] (m5b.north) -- (m2b.south east);
  \draw [->] (m6) -- (m5c);
  \draw [->] (m6) -- (m4c.south east);
\end{tikzpicture}
\caption{Chaining messages by referencing hashes of previous messages.}\label{fig:hash-chain}
\end{figure}

The construction in Figure~\ref{fig:hash-chain} is similar to the internal structure of a Git repository~\cite{Chacon:2014kr}, in which each commit references the hash of one or more parent commits.

\section{Adding New Collaborators}\label{sec:new-member}

The approach in Section~\ref{sec:convergence} ensures convergence in a static group of collaborators, where all members are added when a group is created.
In this setting, every message in the history of the group is delivered to every member device.
However, if the membership is dynamic~-- that is, if group members can be added or removed~-- additional challenges arise.

With most group messaging protocols, when a new member is added, that member is able to receive any messages that were sent after they were added, but no prior messages.
However, in the context of collaboration on some shared data, receiving later messages is not sufficient: the new member also requires a copy of the shared data to which any subsequent updates can be applied.

The simplest solution is to give the new member the full update history: that is, the administrator who invites the new member also sends the new member a copy of all past messages sent in the group.
If a hash-chaining construction like in Figure~\ref{fig:hash-chain} is used, the new member can check the integrity of this message history by computing the hashes of the messages and comparing them to the referenced hashes.

However, sending the full update history has two downsides.
Firstly, it may be much larger than a copy of the current state, and thus inefficient to store and transmit.
Secondly, the full update history includes every past version of the data, including any content that has been deleted and is no longer part of the current state.
For privacy reasons, the existing collaborators may not want to expose the full details of former versions of the data to the new collaborator.

\section{Checking the Integrity of Snapshots}\label{sec:snapshots}

If it is not acceptable to send the full update history to a new collaborator, a snapshot of the current state of the shared data must be sent instead.
However, a naive snapshot protocol would lose the convergence property in the presence of a malicious user: namely, the user who sends the snapshot may send data that does not match the true current state, and thus cause the new collaborator's state to diverge from the rest of the group.
For example, the malicious user could claim that another user wrote something that, in fact, they never wrote.

We are exploring protocols that allow the new collaborator to verify that a snapshot is consistent with the prior editing history of the shared data, without revealing the full update history to the new collaborator.
Approaches include:
\begin{enumerate}
\item The snapshot can be sent to all group members, not just the new collaborator.
Existing group members can then check the integrity of the snapshot, and vote on whether they believe the snapshot to be correct or not.
A Byzantine consensus protocol~\cite{Castro:1999te,Correia:2011bo} can make this voting process resilient to malicious users, provided that a quorum (typically, more than $2/3$) of voting members is honest.
However, this approach requires members to be online in order to vote.
If members use mobile devices that are frequently offline, as proposed in Section~\ref{sec:threatmodel}, a voting protocol may introduce prohibitive delays before the snapshot integrity is confirmed.
\item As an alternative, the new collaborator could initially trust the first snapshot it receives, and then run a consistency-checking protocol in the background.
This protocol would not prevent the new collaborator from seeing an inconsistent snapshot, but it could ensure that any inconsistency is eventually detected, provided that the new collaborator eventually communicates with an honest group member.
This approach is analogous to Certificate Transparency~\cite{RFC6962}, which does not prevent certificate authorities from misissuing certificates, but which deters such behaviour by making it detectable and irrefutable.
\item There may be cryptographic constructions that allow the creator of the snapshot to prove to the new collaborator that the snapshot is consistent with the full message history, without revealing the message history.
For example, we are currently exploring the use of redactable signatures and one-way accumulators~\cite{Benaloh:1993on} for this purpose.
\end{enumerate}

In general, we may differentiate \emph{fail-safe} and \emph{fail-secure} approaches to snapshot integrity checking.
A fail-secure (or fail-closed) approach in this context means that the new collaborator must wait until the integrity of the shared data has been fully verified, e.g.\ using some voting protocol or cryptographic proof, before they are allowed to see it.
On the other hand, a fail-safe (or fail-open) approach would allow the new collaborator to immediately see the snapshot~--- even if it might be incorrect~--- and resolve any inconsistencies after the fact.

\section{Conclusions}

End-to-end encryption is now in regular use by over 1 billion people for secure messaging.
Yet, end-to-end encryption is not currently used by collaborative applications where multiple people modify some shared resource, such as a document or database.
In this paper we have outlined a method of building collaborative apps on top of secure messaging protocols, providing not only confidentiality and integrity in the face of network attackers and malicious servers, but also the properties of closeness and convergence.
Handling the insider threat of a malicious collaborating user is more challenging, and we highlighted snapshot integrity as a particular issue which requires further work to fully address.

\bibliographystyle{splncs04}
\bibliography{references}
\end{document}
